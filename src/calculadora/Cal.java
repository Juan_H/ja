package calculadora;


public class Cal {
	
public int sumar(int num1, int num2) {
	return num1+num2;
}
public int resta(int num1, int num2) {
	return num1- num2;
	
}
public int multiplicar(int num1, int num2) {

	return num1*num2;
}
public float dividir(int num1, int num2) {
	if(num2!=0) {
		return num1/num2;
	}
	else {
		System.out.println("No se pude realizar la division");
	}
	return 0;
	}
	
	public int potencia(int base, int exponente) {
		
		return (int) Math.pow(base, exponente);
	
	}
	public int factorial(int num) {
        if (num < 0) {
            System.out.println("No se puede calcular el factorial de un número negativo");
            return -1; 
        } else if (num == 0 || num == 1) {
            return 1; 
        } else {
            int factorial = 1;
            for (int i = 2; i <= num; i++) {
                factorial *= i;
            }
            return factorial;
        }
}
}